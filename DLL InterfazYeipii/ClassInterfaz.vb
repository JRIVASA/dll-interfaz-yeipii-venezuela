﻿'Fecha de Creación: 30/07/2019

Imports System.Net
Imports System.IO
Imports Newtonsoft.Json
Imports System.Reflection

Public Class Json_resul_Yeipii
    Public success As Boolean
    Public code As String
    Public amount As Object
    Public createdAt As Object
    Public sender As Object 'As Json_Sender
    Public diff As Object
End Class

Public Class Json_Sender
    Public username As String
    Public id_document As String
    Public full_name As String
End Class

Public Class ClassInterfaz

    Public Ambiente As String
    Public User As String
    Public Token As String

    Public Exito As Boolean
    Public Codigo As String
    Public MontoPagado As Object
    Public Fecha As Object
    Public Diferencia As Object

    Public UsuarioYeipii As String
    Public CodigoDocumento As String
    Public Nombre As String

    Public Mensaje As String

    Public Sub ObtenerDatosConfig()
        Try
            'ArchivoConf = Assembly.GetExecutingAssembly.Location.Replace("InterfazYeipii.dll", "YeipiiConf.ini")

            'Ambiente = LeerIni(ArchivoConf, "Config", "Ambiente", "Produccion")
            'User = LeerIni(ArchivoConf, "Config", "User", String.Empty)
            'Token = LeerIni(ArchivoConf, "Config", "Token", String.Empty)

            If Ambiente = "Produccion" Then
                Link = "https://restful.yeipii.com/serverbusiness/business/operations/referencias"
            Else
                Link = "https://trestful.yeipii.com/serverbusiness/business/operations/referencias"
            End If

        Catch ex As Exception
            'MsgBox(Err.Description, , "Error en ObtenerDatosConfig")
            Mensaje = Err.Description
        End Try
    End Sub

    Public Function Verificacion(Referencia As String, Monto As Double, Optional Retorno As Boolean = False) As Boolean
        Try
            ObtenerDatosConfig()

            Dim Request As WebRequest

            Request = WebRequest.Create(Link)

            Request.Method = "POST"
            Request.ContentType = "application/json"

            Request.Headers.Add("Authorization", Token)
            Request.Headers.Add("User", User)

            Dim data As New String("{ ""ref"":""" & Referencia & """, ""amount"":" & Monto & ", ""return"":" & LCase(Retorno) & " }")

            Dim encoding As New System.Text.UTF8Encoding()

            Dim bytes As Byte() = encoding.GetBytes(Data)

            Using requestStream As Stream = Request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using

            Dim response As WebResponse = Request.GetResponse

            If CType(response, HttpWebResponse).StatusCode = HttpStatusCode.OK Then

                Dim dataStream As New StreamReader(response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim JsonResp As Json_resul_Yeipii
                JsonResp = JsonConvert.DeserializeObject(Of Json_resul_Yeipii)(result)

                Exito = JsonResp.success
                Codigo = JsonResp.code
                MontoPagado = JsonResp.amount
                Fecha = JsonResp.createdAt
                Diferencia = JsonResp.diff

                If Not IsNothing(JsonResp.sender) Then
                    Dim JsonRespSender As Json_Sender
                    JsonRespSender = JsonConvert.DeserializeObject(Of Json_Sender)(JsonResp.sender.ToString)

                    UsuarioYeipii = JsonRespSender.username
                    CodigoDocumento = JsonRespSender.id_document
                    Nombre = JsonRespSender.full_name
                End If

                If JsonResp.success = True Then
                    Verificacion = True
                    Select Case Codigo
                        Case "0"
                            Mensaje = "Transacción Exitosa"
                        Case "1"
                            Mensaje = "Transacción Exitosa, el pago fue realizado por un monto mayor al indicado, se realizo devolución automática de la diferencia: " + CStr(Diferencia)
                        Case Else
                            Mensaje = "Transacción Exitosa"
                    End Select
                Else
                    Verificacion = False
                    Select Case Codigo
                        Case "2"
                            Mensaje = "Transacción Fallida, el pago fue realizado por un monto menor al indicado, se realizo la devolucion automática del monto total pagado: " + CStr(MontoPagado)
                        Case "3"
                            Mensaje = "Transacción Fallida, la Referencia introducida Existe, pero ha sido utilizada anteriormente"
                        Case "4"
                            Mensaje = "Transacción Fallida, Referencia introducida no existe"
                        Case Else
                            Mensaje = "Transacción Fallida"
                    End Select
                End If

            End If

            response.Close()

            Return Verificacion

        Catch ex As Exception
            'MsgBox(Err.Description, , "Error en Verificación")
            Mensaje = Err.Description
            Return False
        End Try
    End Function

    Public Sub New()
        Try
            ObtenerDatosConfig()
        Catch ex As Exception
            'MsgBox(Err.Description, , "Error en NewClassInterfaz")
            Mensaje = Err.Description
        End Try
    End Sub

End Class
