﻿Module General

    'Definicion de Variables
    'Public Ambiente As String
    Public ArchivoConf As String
    'Public User As String
    'Public Token As String

    Public Link As String

    'Funciones de la API de Windows
    Private Declare Function GetPrivateProfileStringKey Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function WritePrivateProfileString Lib "Kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer

    Public Function LeerIni(ByVal sIniFile As String, ByVal sSection As String, ByVal Skey As String, ByVal SDefault As String) As String
        Dim sTemp As String
        Dim nLenght As Integer
        sTemp = Space$(10000)
        nLenght = GetPrivateProfileStringKey(sSection, Skey, SDefault, sTemp, 9999, sIniFile)
        LeerIni = Left$(sTemp, nLenght)
    End Function

    Public Sub EscribirIni(ByVal SIniFile As String, ByVal SSection As String, ByVal sKey As String, ByVal sData As String)
        Try
            WritePrivateProfileString(SSection, sKey, sData, SIniFile)
        Catch Any As Exception
            Console.WriteLine(Any.Message)
        End Try
    End Sub

End Module
